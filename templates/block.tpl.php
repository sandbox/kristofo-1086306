<?php
/**
 * @file
 *  block.tpl.php
 *
 * Theme implementation to display a block.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>"<?php if (isset($block_classes)) print ' class="'. $block_classes .'"'; ?>>
  <?php if (!empty($block->subject)): ?>
    <h2 class="block-title"><?php print $block->subject; ?></h2>
  <?php endif; ?>
  <div class="block-content clearfix">
    <?php print $block->content; ?>
  </div>
</div> <!-- /block -->