<?php
/**
 * @file
 *  node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>
<div<?php if ($node_classes) print ' class="'. $node_classes .'"'; ?>>
  <?php print $content; ?>
</div> <!-- /node -->