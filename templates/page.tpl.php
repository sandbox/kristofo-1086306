<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<!--[if lt IE 7 ]> <body class="ie6 <?php print $body_classes; ?>"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7 <?php print $body_classes; ?>"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8 <?php print $body_classes; ?>"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9 <?php print $body_classes; ?>"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body class="<?php print $body_classes; ?>"> <!--<![endif]-->

  <div id="body-inner">
    <div id="page" class="clearfix">

      <div id="header-wrapper" class="clearfix">
        <div id="logo">
          <?php if (!$is_front): ?><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><?php endif; ?>
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          <?php if (!$is_front): ?></a><?php endif; ?>
        </div><!-- /#logo -->
        
        <?php if ($search_box): ?><div id="search"><?php print $search_box ?></div><?php endif; ?>
       
        <div id="header">
          <?php if (!empty($header)){ ?>
            <?php print $header; ?>
          <?php } ?>
        </div>
      </div><!--/#header-wrapper -->

      <div id="navigation" class="clearfix">
        <?php if (isset($primary_links)): ?>
          <?php print theme('links', $primary_links, array('class' => 'menu')); ?>
        <?php endif; ?>
      </div>

      <div id="content-wrapper" class="clearfix">

        <div id="content">
          <div id="content-inner" <?php if (isset($page_classes)) print ' class="'. $page_classes .'"'; ?>>
            <?php // if ($tabs): print '<div id="tabs">'. $tabs .'</div>'; endif; ?>
            <?php if($content_top): ?>
            	<div id="content_top">
            		<?php print $content_top; ?>
            	</div>
            <?php endif; ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
            <?php if ($help): print $help; endif; ?>
            <?php if ($title): print '<h1 class="title">' . $title . '</h1>'; endif;?>
            <?php print $content; ?>
            <?php if($content_bottom): ?>
            	<div id="content_bottom">
            		<?php print $content_bottom; ?>
            	</div>
            <?php endif; ?>
          </div><!--/#content-inner -->
        </div><!--/#content -->

        <?php if ($left): ?>
          <div id="sidebar-first" class="sidebar">
            <?php print $left; ?>
          </div>
        <?php endif; ?>

        <?php if ($right): ?>
          <div id="sidebar-second" class="sidebar">
            <?php print $right; ?>
          </div>
        <?php endif; ?>

      </div><!--/#content-wrapper -->

      <?php if ($footer || $footer_message): ?>
        <div id="footer-wrapper" class="clearfix">
          <div id="footer">
            <?php print $footer; ?>
            <?php print $footer_message; ?>
          </div>
        </div>
      <?php endif; ?>
    </div><!--/#page -->
  </div>
  <?php print $closure; ?>
</body>
</html>