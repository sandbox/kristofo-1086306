<?php
/**
 * Add extra classes to the body:
 * language, node-type, page, section.
 */
function phptemplate_preprocess_page(&$vars, $hook) {
  // Get current classes.
  $body_classes = drupal_map_assoc(explode(" ", $vars['body_classes']));

  if ($vars['language']->language){
    $body_classes[] = 'language-'.$vars['language']->language;
  }

  if (!$vars['is_front']) {
    // Add unique classes for each page and website section.
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = phptemplate_id_safe('page-'. $path);
    $body_classes[] = phptemplate_id_safe('section-'. $section);
  }

  $vars['body_classes'] = implode(' ', $body_classes);

  return $vars;
}

/**
 * Add extra classes to the node.
 */
function phptemplate_preprocess_node(&$vars, $hook) {

  $node_classes = array();

  $node_classes[] = 'node';
  $node_classes[] = 'node-type-'. $vars['node']->type;
  if ($vars['node']->sticky) { 
    $node_classes[] = 'sticky';
  }
  if (!$vars['node']->status) { 
    $node_classes[] = 'node-unpublished';
  }
  $node_classes[] = 'clearfix';
  
  // Merge with current classes.
  $node_classes = array_merge($node_classes, explode(" ", $vars['node_classes']));
  
  $vars['node_classes'] = implode(' ', $node_classes);

  return $vars;
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * - Preceeds initial numeric with 'n' character.
 * - Replaces space and underscore with dash.
 * - Converts entire string to lowercase.
 * - Works for classes too!
 *
 * @param string $string
 *   The string
 * @return
 *   The converted string
 */
function phptemplate_id_safe($string) {
  if (is_numeric($string{0})) {
    // If the first character is numeric, add 'n' in front.
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/**
 * Add extra classes to links.
 */
function phptemplate_links($links, $attributes = array('class' => 'links')) {
  global $base_path;
  $output = '';
  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links.
      $class .= ' item-'.$i;
      if ($i % 2) {
        $class .= ' odd';
      } else {
        $class .= ' even';
      }
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {

        if(!isset($link['attributes']['title'])){
          $link['attributes']['title'] = $link['title'];
        }

        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'],  $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $output .= "</li>\n";

      $i++;
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Form overwrite to add clearfix to the div surrounding the form.
 */
function phptemplate_form($element) {
  $action = $element['#action'] ? 'action="'. check_url($element['#action']) .'" ' : '';
  if (isset($element['#attributes']['class'])){
    $element['#attributes']['class'] .= ' clearfix';
  }
  else {
    $element['#attributes']['class'] = 'clearfix';
  }
  return '<form '. $action .' accept-charset="UTF-8" method="'. $element['#method'] .'" id="'. $element['#id'] .'"'. drupal_attributes($element['#attributes']) .">\n". $element['#children'] ."\n</form>\n";
}

/**
 * Form element overwrite to add the clearfix class.
 */
function phptemplate_form_element($element, $value) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  $output = '<div class="form-item clearfix"';
  if (!empty($element['#id'])) {
    $output .= ' id="'. $element['#id'] .'-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="'. $t('This field is required.') .'">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="'. $element['#id'] .'">'. $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
    else {
      $output .= ' <label>'. $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) ."</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Menu tree overwrite to add the clearfix class.
 */
function phptemplate_menu_tree($tree) {
  return '<ul class="menu clearfix">'. $tree .'</ul>';
}

/**
 * Add extra parameters to item lists,
 * This should be identical to the classes in views-view-list.tpl.php.
 */
function phptemplate_item_list($items = array(), $title = NULL, $type = 'ul', $attributes = NULL) {
  $output = '<div class="item-list clearfix">';
  if (isset($title)) {
    $output .= '<h3>'. $title .'</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type". drupal_attributes($attributes) .'>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        $data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
      }
      $list_id = $i + 1;
      $attributes['class'] = empty($attributes['class']) ? 'item-'. $list_id : ($attributes['class'] .' item-'. $list_id);

      if ($i == 0) {
        $attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] .' first');
      }
      if ($i == $num_items - 1) {
        $attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] .' last');
      }
      if ($i%2) {
        $attributes['class'] = empty($attributes['class']) ? 'even' : ($attributes['class'] .' even');
      } else {
        $attributes['class'] = empty($attributes['class']) ? 'odd' : ($attributes['class'] .' odd');
      }
      $output .= '<li'. drupal_attributes($attributes) .'>'. $data ."</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}