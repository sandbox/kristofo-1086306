// open external links in new window
Drupal.behaviors.openInNewWindow = function(context) {

  // open external links in new window
  $("a[@href^=http]").each(function() {
    if(this.href.indexOf(location.hostname) == -1) {
      $(this).click(function(){window.open(this.href);return false;});
    }
  });

  // open pdf files in new window
  $("a")
  .filter(
    function (index) {
      if ($(this).attr("href")) {
        var href = $(this).attr("href").toLowerCase();
        var hrefArray = href.split('.');
        var ext = hrefArray[hrefArray.length - 1];
        return  ext == 'pdf';
      }
    }
  )
  .each(function() {
    $(this).click(function(){window.open(this.href);return false;});
  });
  
}